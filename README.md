Buatlah suatu project android sederhana dengan tema "To-Do List",  
project cukup terdiri dari satu halaman utama dengan fitur :
1. Menampilkan daftar seluruh agenda
2. Menambah suatu agenda ke dalam To-Do List
3. Mengubah suatu agenda di dalam To-Do List
4. Menghapus suatu agenda di dalam To-Do List

Detail yang perlu diperhatikan :
- Untuk menambah atau mengubah agenda disarankan untuk masuk ke halaman baru.
- To-Do List cukup disimpan ke dalam android, dengan menggunakan SQLite.
- Wajib menggunakan Android Native.
- Usahakan untuk selalu memberikan dokumentasi berupa komentar pada bagian-bagian yang penting.
- Usahakan untuk membuat struktur folder dan file dengan rapi.
